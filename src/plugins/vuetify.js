import Vue from "vue";
import "font-awesome/css/font-awesome.min.css";
import Vuetify from "vuetify";
import i18n from "./i18n.js";

//import '@vuetify'

Vue.use(Vuetify, {
    theme: {
        lang: {
            t: (key, ...params) => i18n.t(key, params),
        },
    },
});

export default new Vuetify({
    icons: {
        iconfont: "fa4", // default - only for display purposes
    },
});