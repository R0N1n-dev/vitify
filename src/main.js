import Vue from "vue";
import "./assets/all.css";
import vuetify from "./plugins/vuetify.js";
import i18n from "./plugins/i18n.js";
import "vuetify/dist/vuetify.min.css";
import { registerSW } from "virtual:pwa-register";
import App from "./App.vue";

const updateSW = registerSW({
  onNeedRefresh() {},
  onOfflineReady() {},
});
new Vue({
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
