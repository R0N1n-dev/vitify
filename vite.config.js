import { defineConfig } from "vite";
import { createVuePlugin } from "vite-plugin-vue2";
import Components from "unplugin-vue-components/vite";
import { VitePWA } from "vite-plugin-pwa";

// import vuetify from './plugins/vuetify.js'

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    brotliSize: false,
    chunkSizeWarningLimit: 900,
  },

  optimizeDeps: {
    include: ["vuetify"],
  },

  plugins: [
    createVuePlugin(),
    Components({}),
    VitePWA({}),
    // vuetify({ compileLib: true }),
  ],
});
