# Intro

Sample project based on Vite 2 + Vuetify + Vue-i18n for localization from resx files

There is no tree shaking for Vuetify - all components are loaded from precompiled vuetify.js

## Clone and build

```bash
git clone git@github.com:alfeg/vite-vuetify.git
cd vite-vuetify
npm ci
npm run build
```
## Run

```bash
npm i
npm run dev
```

## Build

```bash
npm run build
```

## Serve

```bash
npm run serve
```
