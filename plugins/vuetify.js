// import webTypes from 'vuetify/dist/json/web-types.json'

// /**
//  * Plugin to load all vuetify components
//  *
//  * @export
//  * @param {*} { compileLib } - Use vuetify/lib to import components
//  * @returns
//  */
// export default function vuetifyImport({ compileLib }) {
//     const virtualFileId = '@vuetify'

//     compileLib = compileLib || true

//     let collection;

//     return {
//         name: 'vite-vuetify', // required, will show up in warnings and errors

//         buildStart() {
//             collection = "import Vue from 'vue'\r\n"

//             const tags = webTypes.contributions.html.tags.map(t => t.name);

//             collection += 'import { ' + tags.join(',\r\n') + ' } from "vuetify/lib";\r\n';

//             tags.forEach(tag => {
//                 collection += `Vue.component('${tag}', ${tag})\r\n`
//             })
//         },

//         resolveId(id) {
//             if (id === virtualFileId) {
//                 return virtualFileId
//             }
//         },

//         load(id) {
//             if (id === virtualFileId) {
//                 return collection
//             }
//         },
//     }
// }